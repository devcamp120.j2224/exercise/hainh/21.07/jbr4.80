package com.example.demo.ShapeAPI;

public class Rectangle extends Shape{
    private double width = 1.0 ;
    private double length = 1.0 ;
    
    public double getwidth() {
        return width;
    }


    public void setwidth(double width) {
        this.width = width;
    }


    public double getLength() {
        return length;
    }


    public void setLength(double length) {
        this.length = length;
    }


    public Rectangle() {
    }


    public Rectangle(double width, double length) {
        this.width = width;
        this.length = length;
    }


    public Rectangle(String color, boolean filled, double width, double length) {
        super(color, filled);
        this.width = width;
        this.length = length;
    }

    public double getArea() {
        return this.width * this.length;
    }

    public double getPerimeter() {
        return this.width + this.length;
    }


    @Override
    public String toString() {
        return "Rectangle [ Shape [ color= " + getColor() + " , filled= " + isFilled() + "]"  + " , width= " + width + " , length= " + length + "]";
    }

   
}
